<div align="center">
    <h1>Wine Quality Prediction on AWS</h1>
</div>

---

### F1-Score - 0.9709129511677284

---
### Tools

1. **Terraform** was used as an Infrastructure as a Code(IaC) tool to define, build, change and version AWS Resources.
2. **GitLab** was used to store and version source code for IaC and both the object and text recognition part of the requirement.
3. **GitLab CI/CD** was used to create a CI/CD pipeline for the IaC and the the object and text recognition services
4. **Trivy** was used as a container vulnerability scanning tool to find out vulnerabilities in the Images that we built and stored in the GitLab Container Registries and integrate a sense of security into the pipeline.
5. **Docker** was used to containerize our services and deploy them along with their dependencies without having to worry about the setup and environment of the host machine.
6. **MLlib(Apache Spark)** was used to make machine learning scalable and parallelizable.
7. **AWS** was used as our choice of cloud provider to setup the underlying infrastructure for our services and their functionalities.
---
### Cloud Infrastructure Setup

To replicate the infrastructure setup that I have done, we need to make sure the following basic things are in place:

1. The `/project2/iac` directory is uploaded to GitLab along with the `.gitlab-ci.yml` file.
2. The `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` pipeline variables are set in the repository under `Settings -> CI/CD -> Variables`

Once these two conditions are met, we need to run the pipeline from Build -> Pipeline
Once the Validate and Plan jobs pass, you will need to manually run the Apply job to apply the configuration to AWS.

**Note:** Make a note of the server password from line 5 of `/project2/iac/userdata.sh`

---
### Docker Swarm Setup

To setup our training cluster in Swarm mode, we need to do a one time configuration of the cluster  as follows:

1. Log in to the server with the name p2_training0 with its ElasticIP and the password from the previous step using SSH.
2. Run the following command on the p2_training0(master node):
    - `docker swarm init`
3. Once the command successfully runs copy the output line from the result that starts with `docker swarm join-token`.
4. Log in to the servers p2_training1, p2_training2, p2_training3(worker nodes) and paste and run the command from the previous step.
5. If everything is done correctly you should see 4 servers in the list when you run `docker node ls`.
---
### Application Setup

To replicate the application setup that I have done, we need to make sure the following basic things are in place:

1. The Infrastructure setup is successfully done as mentioned above.
2. You have the Elastic IPs for the five EC2 instances.
3. The `/project2/preprocessing`,`/project2/training` and `/project2/prediction` directories have been uploaded to three separate GitLab repositories along with their respective `Dockerfiles`, `requirements.txt` dependency files, `.gitlab-ci.yml` files and `docker-compose.yml` compose files wherever necessary.
4. You have generated a personal access token for your GitLab account as shown in the documentation here: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Once these basic requirements are in place we need to set the CI/CD environment variables in both the repositories:

- Go to `Deploy -> Container Registry` and add the registry URL as a CI/CD variable called **CI_REGISTRY** at `Settings -> CI/CD -> Variables` in your registry.
- Add another variable called **SERVER_IP**
    - For the `preprocessing` and `prediction` repositories we need to add the IP for the Preprocessing EC2 instance.
    - For the  `training` repository we need to add the IP of the Swarm Master node(p2_training0).
- Along with these, also add the `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` variables as done in the IaC repository here again.
- Add your GitLab personal access token as **GITLAB_ACCESS_TOKEN** in your CI/CD variables.
- Add your GitLab userID as **CI_REGISTRY_USER** variable.
- Add the password I asked you to take note of during the IaC setup as **SSH_PASSWORD** variable.
- Add `p2admin` as **SERVER_USER** variable.
- The last thing we need to do is add our ssh key from `AWS Academy -> AWS Details` as **SSH_PRIVATE_KEY** variable.

Once these variables are set, we need to run the pipeline from `Build -> Pipeline` for the *Data Preprocessing*(`/project2/preprocessing`) repository first, then the *Model Training*(`/project2/training`) repository once the Data Preprocessing pipeline is complete and then lastly the *Model Prediction*(`/project2/prediction`) repository once the Model Training pipeline is complete.

---
### Data Pre-processing

To clean and pre-process our data we have taken the following steps:

1. **Feature Selection:** After normalizing the data in the TrainingDataset and generating an information gain graph and correlation matrix <u>locally</u>, I found out that the independent variables `residual sugar` and `free sulfur dioxide` did not impact the value of the target variable (in our case `quality`) much and are hence redundant can be dropped.
2. **Normalization:** I have then normalized the data using `Min-Max scaling` so that the data is transformed into a fixed range(between 0 and 1)
3. **Balancing the data** I have then run `SMOTE` against the Normalized TrainingDataset to balance our data by oversampling the minority class.
4. **Binarizing the Target Variable**: I have then binarized our target variable(quality) such that any value greater than or equal to 7 is replaced with a 1 and all other values of quality are repalced as 0. The logic behind this is that our model needs to differentiate the difference between great and bad wine and any rating above 7 is generally qualified to be great wine.

### Classifier Selection

In my case, the **Random Forest Classifier** seemed to give me the best F1 score after trying out Logistic Regression which gave me a score around 0.75 and SVC which gave me a score around 0.91. This could be because RFC is a great multiclass classifier and is less prone to overfitting while also helping reduce overfitting and increasing accuracy.

---
### Architecture

![Architecture](architecture.png)

---
### Breaking Down The Architecture

#### The IaC Pipleine

The IaC Pipeline has 3 stages

1. **Validate** - To validate the syntax of the code which exits with a status code if there is a syntax error.
2. **Plan** - The most important part of pipeline. It refers to the tf_state file which is stored in GitLabs http backend to maintain the state of the cloud enviroment and track the changes that are made using our repository. It then lets us know whicch resources need to be created, deleted and updated after comparing the current state of the code with that in the tf_state file and outputs a tf_plan file with all this information and passes it to the downstream Apply job.
3. **Apply** - The Apply job is a manual job which which will create, delete or update resources accoring the the tf_plan file that was passes to it by the upstream job. The reason for it being manual is because the plan should be checked properly first as any mistake made while applying could cause serious issues in the cloud enviroment.
3. **Destroy** - Destroy is an optional manual job as a part of the 3rd stage and is independent of Apply. It is just created in case we need to take down our entire infrastructure in one go if ever required.

---
#### The AWS Enviroment

1. **EC2 Instances** - Five AWS EC2 instances of size t3.medium have been created in the public subnet along with

    - A *Security Group* that only allows 
        - `SSH` from <u>anywhere</u>
        - `HTTP` and `HTTPS` from <u>within the subnet only</u>
        - `2377/tcp`, `7946/tcp`, `7946/udp` and `4789/udp` from <u>within the subnet only</u> for the *Swarm Cluster* to communicate.
        - `7077/tcp` and `8080/tcp` from <u>within the subnet only</u> for the *Spark Cluster* to communicate.
    - Five *Elastic IP* addresses for the five Instances as AWS Academy kept reassigning them for every new session and changing the GitLab CI/CD pipeline variables everytime felt like a hassle while testing.
    - The instances were assigned the `LabInstanceProfile` instance profile so that they could communicate with SQS and Rekognition using the access key, secret key and session token.

    The EC2 instances were created with a `userdata script` for first time setups that enabled password authentication, set a password for the p2admin, gave the p2admin sudo privileges, installed docker to run our containers and allowed docker to run without sudo for the p2admin user.

2. **S3** - Used to store and retrieve our preprocessed datasets and trained models.

---
#### The Application Pipelines (with Docker)

##### <li>The stages for both the <u>preprocssing</u> and <u>prediction</u> applications are the same and have 3 stages:</li>

1. **Build Image** - This job passes the dependency file(requirements.txt file), python code and other artifacts(Datasets in case of the preprocessing application) to the *Dockerfile* to build the image for our container. Once this image is packaged, I have stored it on GitLabs container registry where we can easily manage it.
2. **Image Scan** - In this stage, I have made use *Trivy*(https://trivy.dev/) which is an open source security scanning tool that can be used to scan containers for vulnerabilities in the operating system or installed packages. This helps me add a layer of security to the lifecycle of the application.
3. **Deploy** - In this stage we deploy the application to the specific server using *SSH* and pass in the AWS credentials as environment variables to the container at runtime from the CI/CD environment variables.
    - The <u>preprocessing</u> application once run pushed the preprocessed Training and Validation datasets to our S3 bucket.
    - The <u>prediction</u> application pulls the trained model from S3, runs it against the Validation dataset and outputs the F1 score to the server.

##### <li>The <u>training</u> application has 3 stages:</li>

1. **Deploy Spark Cluster** - This job
    - copies the `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` from the GitLab environment into a aws.env file.
    - copies all the files from the repository and the aws.env file to the server,
    - gives executable permissions to the shell scripts and
    - deploys the Spark Cluster on the Swarm Master Node as a stack using the `docker-compose.yml` file which also sets the variables from aws.env into each container. Deploying it as a stack allows Docker Swarm to automatically load balance the containers as per the servers resource utilization and the placement configuration in the compose file.
2. **Train Model** - This stage of the pipeline 
    - uses the `vars.sh` script to dynamically retrieve the container IDs of the spark-master and one of the spark-workers that is configured to run on the Swarm Manager as per the placement configuration and sets them as environment variables,
    - it then runs the `train.sh` script which 
        - installs numpy on the spark-worker as it is a requirment for our code to run
        - submits the python file to the spark-master using `spark-submit --master <sparkMasterurl:7077> predict.py` which then lets spark handle the training of the model parallely by distributing the task among the four spark-worker containers in the cluster
        - once the model is trained successfully, it is pushed to S3.
3. **Environment Cleanup** - The environment cleanup stage is actually included in the previous stage but as it serves a separte purpose, I have included it separately. This stage
    - undeploys the Spark Cluster from the server
    - removes all the files that were moved to the server during stage 1 for security purposes

---
### Running the Prediction Application without Docker(Ubuntu - comes pre-installed with Python)

1. Update the system packages
    - `sudo apt update`
2. Install the Java Development Kit(JDK)
    - `sudo apt install default-jdk`
3. Run the following commands to Download and Install Spark
    - `wget https://dlcdn.apache.org/spark/spark-3.4.0/spark-3.4.0-bin-hadoop3.tgz`
    - `tar xvf spark-3.4.0-bin-hadoop3.tgz`
    - `sudo mv spark-3.4.0-bin-hadoop3 /opt/spark`
4. Configure Environment Variables
    - `nano ~/.bashrc`
    - Add the following lines to the end of the file
        - `export SPARK_HOME=/opt/spark`
        - `export PATH=$PATH:$SPARK_HOME/bin`
        - `export AWS_ACCESS_KEY_ID=<your AWS_ACCESS_KEY_ID from AWS Academy>`
        - `export AWS_SECRET_ACCESS_KEY=<your AWS_SECRET_ACCESS_KEY from AWS Academy>`
        - `export AWS_SESSION_TOKEN=<your AWS_SESSION_TOKEN from AWS Academy>`
    - Save the file and exit nano
    - `source ~/.bashrc`
5. Verify the Installation
    - `spark-shell`
6. Clone the git repository
    - `git clone <git repo url>`
7. Install dependencies using pip
    - `pip install pyspark numpy`
8. Submit the Python file to Spark
    - `spark-submit </path/to/predict.py/from/the/git/repo>`
---